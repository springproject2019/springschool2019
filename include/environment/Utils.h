#pragma once
#include <chrono/assets/ChColor.h>
#include <chrono/geometry/ChBox.h>

namespace
{
	const double g_wallWidth = 1;
	const double g_wallHeight = 1;
	const double g_wallDepth = 0.04;
	const double g_wallDensity = 10;
	const bool g_isEnabledCollisionDetection = true;
	const bool g_isEnabledVisualizationAsset = true;
	const chrono::ChColor g_wallColor(0.2f, 0.5f, 0.9f);
	const std::string g_texture = "..\\data\\concrete.jpg";

	const double g_bocWidth = 0.1;
	const double g_boxHeight = 0.1;
	const double g_boxDepth = 0.1;
	const double g_boxDensity = 0;
	const double g_boxMass = 2;
	const chrono::ChColor g_boxColor(0.9f, 0.3f, 0.2f);

	const std::string g_beamNodesFile = "..\\data\\fea\\beam.node";
	const std::string g_beamElementsFile = "..\\data\\fea\\beam.ele";
	const chrono::ChVector<> g_beamStartPosition(0, 0, -0.02);
	const uint8_t g_beamBoxes = 10;  // maximum boxes for the beam = 25
	const uint8_t g_noTetahedrons = 6;

	const double g_sphereSweptThickness = 0.002;
	const double g_boxSize = 0.005;

	const chrono::ChVector<> g_activeForceOnABeamNode = chrono::ChVector<>(0, 3, -10);
}
