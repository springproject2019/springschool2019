#pragma once
#include <memory>
#include <vector>
#include <chrono/fea/ChContactSurfaceMesh.h>
#include <chrono/fea/ChNodeFEAxyz.h>
#include <chrono/fea/ChElementTetra_4.h>

class Beam
{
public:
  Beam();
  ~Beam() = default;

  std::shared_ptr<chrono::fea::ChMesh> getBeam() const;
  std::vector<std::shared_ptr<chrono::fea::ChElementTetra_4>> getTetrahedrons() const;

  void addContactSurface(std::shared_ptr<chrono::fea::ChContactSurfaceMesh>& a_contactSurface);
  void applyForceOnANode();
  void visualizeBeam();
  void updateStrainedTetrahedronDimensions(const std::shared_ptr<chrono::fea::ChElementTetra_4>& a_leastStrained, const std::shared_ptr<chrono::fea::ChElementTetra_4>& a_mostStrained);

private:
  void beamGenerator();
  void readNodesFromFile(std::vector<std::shared_ptr<chrono::fea::ChNodeFEAxyz>>& a_nodes);
  void readElementsFromFile(std::vector<std::vector<int>>& a_elements);

private:
  std::shared_ptr<chrono::fea::ChMesh> m_beam;
  std::vector<std::shared_ptr<chrono::fea::ChElementTetra_4>> m_tetrahedrons;
};

