#pragma once
#include <memory>
#include <chrono/physics/ChBodyEasy.h>
#include <chrono/fea/ChContactSurfaceMesh.h>


class Wall
{
public:
	Wall();
	~Wall() = default;

	std::shared_ptr<chrono::ChBodyEasyBox> getWall() const;
	void setMaterialSurface(std::shared_ptr<chrono::ChMaterialSurfaceSMC>& a_surfaceMaterial);

private:
	std::shared_ptr<chrono::ChBodyEasyBox> m_wall;
};

