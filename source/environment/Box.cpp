#include <environment/Box.h>
#include <environment/Utils.h>

#include <chrono/fea/ChBeamSection.h>
#include <chrono/fea/ChBuilderBeam.h>
#include <chrono/fea/ChMesh.h>
#include <chrono/fea/ChVisualizationFEAmesh.h>
#include <chrono/physics/ChSystemNSC.h>
#include <chrono_irrlicht/ChIrrApp.h>
#include <chrono/fea/ChLinkPointFrame.h>
#include <chrono/fea/ChLinkPointPoint.h>

Box::Box()
{
  m_box = std::make_shared<chrono::ChBodyEasyBox>(
    g_bocWidth
    , g_boxHeight
    , g_boxDepth
    , g_boxDensity
    , g_isEnabledCollisionDetection
    , g_isEnabledVisualizationAsset);

  m_box->SetMass(2);

  auto l_texture = std::make_shared<chrono::ChTexture>();
  l_texture->SetTextureFilename(chrono::GetChronoDataFile(g_texture));
  m_box->AddAsset(l_texture);

  auto l_color = std::make_shared<chrono::ChColorAsset>();
  l_color->SetColor(g_boxColor);
  m_box->AddAsset(l_color);
}


void Box::attachToBeam(std::shared_ptr<chrono::fea::ChMesh>& a_beam, chrono::ChSystemNSC& a_chronoSystem)
{
  auto l_mesh = std::make_shared<chrono::fea::ChMesh>();

  auto l_lastBeamNode = std::dynamic_pointer_cast<chrono::fea::ChNodeFEAxyz>(a_beam->GetNode(a_beam->GetNnodes() - 1));
  auto l_lastBeamNode2 = std::dynamic_pointer_cast<chrono::fea::ChNodeFEAxyz>(a_beam->GetNode(a_beam->GetNnodes() - 6));

  m_box->SetPos(l_lastBeamNode->GetPos() + chrono::ChVector<>(0, -0.3, 0));
  a_chronoSystem.Add(m_box);

  createLink(a_chronoSystem, l_lastBeamNode, l_mesh);
  createLink(a_chronoSystem, l_lastBeamNode2, l_mesh);

  a_chronoSystem.Add(l_mesh);

  auto l_visualizeMesh = std::make_shared<chrono::fea::ChVisualizationFEAmesh>(*(l_mesh));
  l_visualizeMesh->SetFEMdataType(chrono::fea::ChVisualizationFEAmesh::E_PLOT_NODE_ACCEL_NORM);
  l_visualizeMesh->SetColorscaleMinMax(-0.5, 0.50);
  l_visualizeMesh->SetShrinkElements(true, 0.85);
  l_visualizeMesh->SetSmoothFaces(true);
  l_mesh->AddAsset(l_visualizeMesh);
}

void Box::createLink(chrono::ChSystemNSC & a_chronoSystem, const std::shared_ptr<chrono::fea::ChNodeFEAxyz>& a_node, std::shared_ptr<chrono::fea::ChMesh>& a_mesh)
{
  auto l_sectionCable = std::make_shared<chrono::fea::ChBeamSectionCable>();
  l_sectionCable->SetDiameter(0.005);
  l_sectionCable->SetYoungModulus(0.01e9);
  l_sectionCable->SetBeamRaleyghDamping(0.000);

  chrono::fea::ChBuilderBeamANCF l_builder;

  l_builder.BuildBeam(a_mesh, l_sectionCable, 1, a_node->GetPos(), m_box->GetPos());
  
  auto l_constraintWithBeam = std::make_shared<chrono::fea::ChLinkPointPoint>();
  l_constraintWithBeam->Initialize(l_builder.GetLastBeamNodes().front(), a_node);
  a_chronoSystem.Add(l_constraintWithBeam);

  auto l_constraintWithBox = std::make_shared<chrono::fea::ChLinkPointFrame>();
  l_constraintWithBox->Initialize(l_builder.GetLastBeamNodes().back(), m_box);
  a_chronoSystem.Add(l_constraintWithBox);
}
