#include <environment/Wall.h>
#include <environment/Utils.h>

#include <chrono_irrlicht/ChIrrApp.h>

Wall::Wall()
{
	m_wall = std::make_shared<chrono::ChBodyEasyBox>(
		g_wallWidth
		, g_wallHeight
		, g_wallDepth
		, g_wallDensity
		, g_isEnabledCollisionDetection
		, g_isEnabledVisualizationAsset);

	m_wall->SetBodyFixed(true);

	auto l_texture = std::make_shared<chrono::ChTexture>();
	l_texture->SetTextureFilename(chrono::GetChronoDataFile(g_texture));
	m_wall->AddAsset(l_texture);

	auto l_color = std::make_shared<chrono::ChColorAsset>();
	l_color->SetColor(g_wallColor);
	m_wall->AddAsset(l_color);
}

std::shared_ptr<chrono::ChBodyEasyBox> Wall::getWall() const
{
	return m_wall;
}

void Wall::setMaterialSurface(std::shared_ptr<chrono::ChMaterialSurfaceSMC>& a_surfaceMaterial)
{
	m_wall->SetMaterialSurface(a_surfaceMaterial);
}
