#include <environment/MeshExporter.h>

void MeshExporter::writeMesh(std::shared_ptr<chrono::fea::ChMesh> my_mesh, std::string SaveAs) {
	std::ofstream MESH;  // output file stream
	MESH.open(SaveAs, std::ios::out);
	MESH.precision(7);
	MESH << std::scientific;

	std::vector<std::vector<int>> TetraElemNodes;

	std::vector<std::shared_ptr<chrono::fea::ChNodeFEAbase>> myvector;
	myvector.resize(my_mesh->GetNnodes());

	for (unsigned int i = 0; i < my_mesh->GetNnodes(); i++) {
		myvector[i] = std::dynamic_pointer_cast<chrono::fea::ChNodeFEAbase>(my_mesh->GetNode(i));
	}

	int numTetra = 0;

	for (unsigned int iele = 0; iele < my_mesh->GetNelements(); iele++) {
		if (auto element = std::dynamic_pointer_cast<chrono::fea::ChElementTetra_4>(my_mesh->GetElement(iele)))
			numTetra++;
	}

	MESH << "\nCELLS " << my_mesh->GetNelements() << " "
		<< (unsigned int)(numTetra * 5) << "\n";

	for (unsigned int iele = 0; iele < my_mesh->GetNelements(); iele++) {
		std::vector<int> mynodes;

		if (auto element = std::dynamic_pointer_cast<chrono::fea::ChElementTetra_4>(my_mesh->GetElement(iele))) {
			mynodes.resize(4);
			MESH << "4 ";
			int nodeOrder[] = { 0, 1, 2, 3 };
			mynodes[0] = element->GetNodeN(nodeOrder[0])->GetIndex();
			mynodes[1] = element->GetNodeN(nodeOrder[1])->GetIndex();
			mynodes[2] = element->GetNodeN(nodeOrder[2])->GetIndex();
			mynodes[3] = element->GetNodeN(nodeOrder[3])->GetIndex();

			TetraElemNodes.push_back(mynodes);
			for (int myNodeN = 0; myNodeN < mynodes.size(); myNodeN++) {
				auto nodeA = (element->GetNodeN(nodeOrder[myNodeN]));
				std::vector<std::shared_ptr<chrono::fea::ChNodeFEAbase>>::iterator it;
				it = find(myvector.begin(), myvector.end(), nodeA);
				if (it != myvector.end()) {
					auto index = std::distance(myvector.begin(), it);
					MESH << (unsigned int)index << " ";
				}
			}
			MESH << "\n";
		}
	}

	MESH << "\nCELL_TYPES " << my_mesh->GetNelements() << "\n";

	for (unsigned int iele = 0; iele < my_mesh->GetNelements(); iele++) {
		if (auto element = std::dynamic_pointer_cast<chrono::fea::ChElementTetra_4>(my_mesh->GetElement(iele)))
			MESH << "10\n"; // 10 = vtk_tetra
	}

	MESH.close();
	// MESH.write_to_file(SaveAs);
}

void evaluateDeflection(double& def, std::shared_ptr<chrono::fea::ChElementTetra_4>& element)
{
	chrono::ChMatrixDynamic<> matrix;
	std::vector<chrono::ChVector<>> vectors;
	chrono::ChVector<> pos;
	chrono::ChVector<> defVec;

	element->GetStateBlock(matrix);
	vectors.push_back(chrono::ChVector<>(matrix.ClipVector(0, 0)));
	vectors.push_back(chrono::ChVector<>(matrix.ClipVector(3, 0)));
	vectors.push_back(chrono::ChVector<>(matrix.ClipVector(6, 0)));
	vectors.push_back(chrono::ChVector<>(matrix.ClipVector(9, 0)));

	for (const auto& vect : vectors)
	{
		pos.x() += vect.x();
		pos.y() += vect.y();
		pos.z() += vect.z();
	}

	defVec = pos / 4;
	def = defVec.Length();
}

void MeshExporter::writeFrame(std::shared_ptr<chrono::fea::ChMesh> a_mesh, std::string SaveAsBuffer, std::string MeshFileBuffer) {
	std::ofstream output;
	std::string SaveAsBuffer_string(SaveAsBuffer);
	SaveAsBuffer_string.erase(SaveAsBuffer_string.length() - 4, 4);
	std::cout << SaveAsBuffer_string << std::endl;
	output.open(SaveAsBuffer, std::ios::app);

	output << "# vtk DataFile Version 2.0" << std::endl;
	output << "Unstructured Grid Example" << std::endl;
	output << "ASCII" << std::endl;
	output << "DATASET UNSTRUCTURED_GRID" << std::endl;

	output << "POINTS " << a_mesh->GetNnodes() << " float\n";
	for (unsigned int i = 0; i < a_mesh->GetNnodes(); i++) {
		auto node = std::dynamic_pointer_cast<chrono::fea::ChNodeFEAxyz>(a_mesh->GetNode(i));
		output << node->GetPos().x() << " " << node->GetPos().y() << " " << node->GetPos().z() << "\n";
	}

	std::ifstream CopyFrom(MeshFileBuffer);
	output << CopyFrom.rdbuf();

	int numCell = 0;
	for (unsigned int iele = 0; iele < a_mesh->GetNelements(); iele++) {
		if (auto element = std::dynamic_pointer_cast<chrono::fea::ChElementTetra_4>(a_mesh->GetElement(iele)))
			numCell++;
	}

	output << "\nCELL_DATA " << numCell << "\n";
	output << "SCALARS Deflection float 1\n";
	output << "LOOKUP_TABLE default\n";

	double scalar = 0;
	for (unsigned int iele = 0; iele < a_mesh->GetNelements(); iele++)
	{
		if (auto element = std::dynamic_pointer_cast<chrono::fea::ChElementTetra_4>(a_mesh->GetElement(iele)))
			evaluateDeflection(scalar, element);

		output << scalar + 1e-20 << "\n";
	}

	output << "VECTORS Strain float\n";
	chrono::fea::ChStrainTensor<> strainTensor;
	for (unsigned int iele = 0; iele < a_mesh->GetNelements(); iele++) {
		if (auto element = std::dynamic_pointer_cast<chrono::fea::ChElementTetra_4>(a_mesh->GetElement(iele)))
			strainTensor = element->GetStrain();

		output << strainTensor.XX() << " " << strainTensor.YY() << " " << strainTensor.ZZ() << "\n";
	}

	output << "\nPOINT_DATA " << a_mesh->GetNnodes() << "\n";

	output << "VECTORS Velocity float\n";
	for (unsigned int i = 0; i < a_mesh->GetNnodes(); i++) {
		chrono::ChVector<> vel = std::dynamic_pointer_cast<chrono::fea::ChNodeFEAxyz>(a_mesh->GetNode(i))->GetPos_dt();
		vel += chrono::ChVector<>(1e-20);
		output << (double)vel.x() << " " << (double)vel.y() << " " << (double)vel.z() << "\n";
	}

	output << "VECTORS Acceleration float\n";

	for (unsigned int i = 0; i < a_mesh->GetNnodes(); i++) {
		chrono::ChVector<> acc = std::dynamic_pointer_cast<chrono::fea::ChNodeFEAxyz>(a_mesh->GetNode(i))->GetPos_dtdt();
		acc += chrono::ChVector<>(1e-20);
		output << (double)acc.x() << " " << (double)acc.y() << " " << (double)acc.z() << "\n";
	}

	output.close();
}

