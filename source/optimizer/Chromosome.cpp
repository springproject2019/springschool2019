#include <optimizer/Chromosome.h>

#include <iostream>

void Chromosome::setGene(const int& a_position, const char& a_chromosome)
{
	m_chromosome[a_position] = a_chromosome == '1';
}

char Chromosome::getGene(const int& a_position) const
{
	return m_chromosome.to_string()[UtilsOptimizer::g_chromosomeSize - a_position - 1];
}

std::string Chromosome::getChromosome() const
{
	return m_chromosome.to_string();
}

void Chromosome::printChromosome() const
{
	for (auto l_position = 0; l_position < UtilsOptimizer::g_chromosomeSize; ++l_position)
	{
		std::cout << m_chromosome[l_position] << " ";
	}
	std::cout << std::endl;
}

int Chromosome::getHexadecimalValue() const
{
	return m_chromosome.to_ulong();
}

double Chromosome::getValueAcordingToInteval(const int& a_lowestValueFromInterval, const int& a_highestValueFromInterval) const
{
	return a_lowestValueFromInterval
		+ getHexadecimalValue()
		* (a_highestValueFromInterval - a_lowestValueFromInterval)
		/ (pow(2, UtilsOptimizer::g_chromosomeSize) - 1);
}