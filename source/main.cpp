#pragma once

#include <environment/Wall.h>
#include <environment/Beam.h>
#include <environment/Utils.h>
#include <environment/Box.h>
#include <environment/BeamStrains.h>
#include <environment/MeshExporter.h>

#include <chrono/physics/ChContinuumMaterial.h>
#include <chrono/solver/ChSolverMINRES.h>
#include <chrono_irrlicht/ChIrrApp.h>
#include <chrono/fea/ChContactSurfaceMesh.h>
#include <chrono/fea/ChLinkPointFrame.h>

#include <optimizer/GeneticAlgorithm.h>

void createConstraintsBetweenWallAndBeam(chrono::ChSystemNSC& a_chronoSystem, const std::shared_ptr<chrono::ChBodyEasyBox>& a_wall, const std::shared_ptr<chrono::fea::ChMesh>& a_beam)
{
  for (unsigned int index = 0; index < a_beam->GetNnodes(); ++index)
  {
    if (auto l_beamNode = std::dynamic_pointer_cast<chrono::fea::ChNodeFEAxyz>(a_beam->GetNode(index))) {
      if (l_beamNode->GetPos().z() < g_wallDepth)
      {
        auto l_constraint = std::make_shared<chrono::fea::ChLinkPointFrame>();
        l_constraint->Initialize(l_beamNode, a_wall);
        a_chronoSystem.Add(l_constraint);

        // Attach small cube to show the constraint
        auto l_box = std::make_shared<chrono::ChBoxShape>();
        l_box->GetBoxGeometry().Size = chrono::ChVector<>(g_boxSize);
        l_constraint->AddAsset(l_box);
      }
    }
  }
}

int main(int argc, char* argv[])
{
  // Create a Chrono::Engine physical system
  chrono::ChSystemNSC l_chronoSystem;

  // Create the Irrlicht visualization (open the Irrlicht device, bind a simple user interface, etc. etc.)
  chrono::irrlicht::ChIrrApp l_application(&l_chronoSystem, L"This looks pretty cool!", irr::core::dimension2d<irr::u32>(800, 600), false, true);

  // Easy shortcuts to add camera, lights, logo and sky in Irrlicht scene:
  l_application.AddTypicalSky();
  l_application.AddTypicalLights();
  l_application.AddTypicalCamera(irr::core::vector3df(0, (irr::f32)0.6, 1));

  // Initialize a surface material
  auto l_surfaceMaterial = std::make_shared<chrono::ChMaterialSurfaceSMC>();
  l_surfaceMaterial->SetYoungModulus(6e4);
  l_surfaceMaterial->SetFriction(0.3f);
  l_surfaceMaterial->SetRestitution(0.2f);
  l_surfaceMaterial->SetAdhesion(0);

  // Create a contact surface for the beam
  auto l_contactSurface = std::make_shared<chrono::fea::ChContactSurfaceMesh>();

  Wall l_wall;
  l_wall.setMaterialSurface(l_surfaceMaterial);
  l_chronoSystem.Add(l_wall.getWall());

  Beam l_beam;
  l_beam.addContactSurface(l_contactSurface);
  //l_beam.applyForceOnANode();
  l_chronoSystem.Add(l_beam.getBeam());

  l_contactSurface->AddFacesFromBoundary(g_sphereSweptThickness);
  l_contactSurface->SetMaterialSurface(l_surfaceMaterial);

  Box l_box;
  l_box.attachToBeam(l_beam.getBeam(), l_chronoSystem);

  // Create constraints
  createConstraintsBetweenWallAndBeam(l_chronoSystem, l_wall.getWall(), l_beam.getBeam());

  // Visualization of the beam
  l_beam.visualizeBeam();

  // Draw items
  l_application.AssetBindAll();
  l_application.AssetUpdateAll();

  // Setup Chrono::Engine
  l_chronoSystem.SetupInitial();
  l_chronoSystem.SetTimestepperType(chrono::ChTimestepper::Type::EULER_IMPLICIT_LINEARIZED);
  l_chronoSystem.SetSolverType(chrono::ChSolver::Type::MINRES);
  l_chronoSystem.SetSolverWarmStarting(true);
  l_chronoSystem.SetMaxItersSolverSpeed(40);
  l_chronoSystem.SetTolForce(1e-10);

  l_application.SetTimestep(0.1);

  MeshExporter::writeMesh(l_beam.getBeam(), "test");
  MeshExporter::writeFrame(l_beam.getBeam(), "test0.vtk", "test");

  int frame = 0;
  // Run the application
  while (l_application.GetDevice()->run())
  {
    l_application.BeginScene();
    l_application.DrawAll();
    l_application.DoStep();
    l_application.EndScene();

    if (frame > 200)
    {
      MeshExporter::writeFrame(l_beam.getBeam(), "test" + std::to_string(frame) + ".vtk", "test");

      break;
    }
    frame++;
  }

  auto l_leastStrainedTetra = getLeastStressedTetra(l_beam.getTetrahedrons());
  auto l_mostStrainedTetra = getMostStressedTetra(l_beam.getTetrahedrons());

  logStressedTetrahedrons(l_leastStrainedTetra, l_mostStrainedTetra);

  l_beam.updateStrainedTetrahedronDimensions(l_leastStrainedTetra, l_mostStrainedTetra);

  MeshExporter::writeMesh(l_beam.getBeam(), "test2");
  MeshExporter::writeFrame(l_beam.getBeam(), "finalTest.vtk", "test2");

  // Run genetic algorithm for one or two variables
  const uint16_t c_numberOfIterations = 3;
  for (int index = 0; index < c_numberOfIterations; ++index)
  {
    GeneticAlgorithm l_geneticAlgorithm;

    std::string l_filename = static_cast<std::string>("SingleVar") + std::to_string(index + 1) + static_cast<std::string>(".txt");

    l_geneticAlgorithm.run(l_filename, 1, false);
  }
  
  for (int index = 0; index < c_numberOfIterations; ++index)
  {
    GeneticAlgorithm l_geneticAlgorithm;
    std::string l_filename = static_cast<std::string>("DoubleVar") + std::to_string(index + 1) + static_cast<std::string>(".txt");

    l_geneticAlgorithm.run(l_filename, 2, true);
  }

  return 0;
}
