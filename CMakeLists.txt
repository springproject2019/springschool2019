cmake_minimum_required (VERSION 2.6)

project (AMMO)

# add the binary tree to the search path for include files
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/include")

add_subdirectory (source) 

#set start-up project
set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT AMMO)