#pragma once

#include <cmath>
#include <stdint.h>
#include <random>

#define M_PI 3.14159265358979323846
#define M_E 2.71828182845904523536

namespace UtilsOptimizer
{
	const int g_epochs = 200;
	const double g_crossoverProbability = 0.4;
	const double g_mutationProbability = 0.1;
	const uint16_t g_populationSize = 100;
	const uint16_t g_chromosomeSize = 10;

	const int g_lowestValueFromInterval = -10;
	const int g_highestValueFromInterval = 10;

	const int g_lowestValueFromIntervalX = -5;
	const int g_highestValueFromIntervalX = 3;

	const int g_lowestValueFromIntervalY = 2;
	const int g_highestValueFromIntervalY = 10;

	static double g_functionSingleVariable(const double& x)
	{
		return x * x  + 6 * x + 1;
	}

	static double g_functionTwoVariables(const double& x, const double& y)
	{
		return std::sin(M_PI * 10 * x + 10 / (1 + y * y)) + std::log(x * x + y * y);
	}

	static std::random_device g_randomDevice;
	static std::mt19937 g_generator(g_randomDevice());

	static std::uniform_real_distribution<> g_randomRealValue(0, 1);
	static std::uniform_int_distribution<> g_randomIntegerValue(0, g_chromosomeSize - 1);
	static std::uniform_int_distribution<> g_randomBinaryValue(0, 2);
}