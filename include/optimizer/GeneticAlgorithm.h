#pragma once
#include <optimizer/Population.h>

class GeneticAlgorithm
{
public:
	GeneticAlgorithm() = default;
	~GeneticAlgorithm() = default;

	void run(std::string a_filename, const uint16_t& a_individualsSize, const bool& a_isMaximizationRequired);

private:
	void select(const uint16_t& a_individualsSize, const bool& a_isMaximizationRequired);
	void crossover(const uint16_t& a_individualsSize, const bool& a_isMaximizationRequired);
	void mutate(const uint16_t& a_individualsSize, const bool& a_isMaximizationRequired);

private:
	Population m_population{};
};
