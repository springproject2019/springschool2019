#pragma once
#include <optimizer/UtilsOptimizer.h>
#include <bitset>

class Chromosome
{
public:
	Chromosome() = default;
	~Chromosome() = default;

	void setGene(const int& a_position, const char& a_chromosome);
	char getGene(const int& a_position) const;
	std::string getChromosome() const;

	void printChromosome() const;
	double getValueAcordingToInteval(const int& a_lowestValueFromInterval, const int& a_highestValueFromInterval) const;

private:
	int getHexadecimalValue() const;

private:
	std::bitset<UtilsOptimizer::g_chromosomeSize> m_chromosome{};
};
