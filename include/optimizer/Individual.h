#pragma once

#include <optimizer/UtilsOptimizer.h>
#include <optimizer/Chromosome.h>

#include <bitset>
#include <vector>

class Individual
{
public:
	Individual() { m_chromosomes.push_back(Chromosome()); m_chromosomes.push_back(Chromosome()); }
	~Individual() = default;

	void setChromosomeGene(const int& a_chromosomeIndex, const int& a_genePosition, const char& a_geneValue);
	void setFitnessValue(const double& a_value);
	void setSelectionProbability(const double& a_value);
	void setSelectionCumulativeProbability(const double& a_value);
	void printChromosome(const int& a_chromosomeIndex) const;

	double getValueAcordingToInteval(const int& a_chromosomeIndex, const int& a_individualsSize) const;
	double getFitnessValue() const;
	double getSelectionProbability() const;
	double getSelectionCumulativeProbability() const;
	char getChromosomeGene(const int& a_chromosomeIndex, const int& a_genePosition) const;
	std::string getChromosome(const int& a_chromosomeIndex) const;

private:
	std::vector<Chromosome> m_chromosomes{};
	double m_fitnessValue{};
	double m_selectionProbability{};
	double m_selectionCumulativeProbability{};
};
