#include "chrono/fea/ChElementTetra_4.h"
#include "chrono/fea/ChMesh.h"
#include "chrono/fea/ChNodeFEAxyz.h"


/// @addtogroup fea_utils
/// @{

/// Collection of mesh file writer utilities.

class MeshExporter {
public:
	/// This function is used to write the connectivity and information of a ChMesh
	/// Currently it only supports ChElementCableANCF, ChElementShellANCF, ChElementBrick
	/// This connectivity is usually constant throughout the simulation. Hence, it may be
	/// called once at the beginning of the simulation (or as many times as it is needed),
	/// and should be used in the later stage when the nodal/elemental informations are updated.
	static void writeMesh(std::shared_ptr<chrono::fea::ChMesh> a_mesh,  ///< destination mesh
		std::string SaveAs                ///< name of the mesh file
	);

	/// The task of this function is to write the information of a ChMesh into a file with vtk format.
	/// The vtk files may be opened with external visualization software such as Paraview, Visit, etc.
	/// This function requires the mesh connectivity file, which may be written by ChMeshExporter::writeMesh function
	/// Note that this function writes the current state of the mesh when is called. It writes some nodal
	/// informations (see the implementation) and elemental informations at the "element center". The elemental
	/// information may be converted to nodal information later on in the post-processing stage with a visualization
	/// software like Paraview.
	static void writeFrame(std::shared_ptr<chrono::fea::ChMesh> a_mesh,  ///< destination mesh
		std::string SaveAsBuffer,           ///< name of the vtk file to export data to
		std::string MeshFileBuffer  ///< name of the mesh file written by ChMeshExporter::writeMesh
	);
};

