#pragma once
#include <optimizer/Individual.h>
#include <vector>

#include <iostream>

class Population
{
public:
	Population() = default;
	~Population() = default;

	void generatePopulation(const uint16_t& a_individualsSize);
	void calculateIndividualFitness(const uint16_t& a_individualsSize);
	void calculateIndividualSelectionProbability(const bool& a_isMaximizationRequired);
	void calculateIndividualSelectionCumulativeProbability();
	void setPopulation(const std::vector<Individual>& a_individuals);
	void storeTheBestResult(const bool& a_isMaximizationRequired);
	void crossover(const std::vector<double>& a_randomValues, const uint16_t& a_individualsSize);
	void mutate(const uint16_t& a_individualsSize);

	std::vector<Individual> getSuitableIndividualsForTheNewGeneration(const bool& a_isMaximizationRequired) const;
	std::vector<Individual> getPop() { return m_population; }
	Individual getExpectedIndividual() const;

private:
	void crossTwoChromosomes(Individual& a_firstChromosome, Individual& a_secondChromosome, const uint16_t& a_individualsSize);
	
	double getSumIndividualFitness() const;
	std::vector<int> getIndividualsForCrossingover(const std::vector<double>& a_randomValues);
	double getMaxValue(const double a_value) const;
private:
	std::vector<Individual> m_population{};

	double m_value{};
	Individual m_individual{};
};
