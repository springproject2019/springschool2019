#pragma once

#include <memory>
#include <chrono/fea/ChElementTetra_4.h>
#include <chrono/fea/ChMesh.h>
#include <chrono/core/ChVector.h>
#include <chrono/physics/ChTensors.h>

std::shared_ptr<chrono::fea::ChElementTetra_4> getLeastStressedTetra(const std::vector<std::shared_ptr<chrono::fea::ChElementTetra_4>>& a_tetrahedrons)
{
  auto l_leastStressedTetra = a_tetrahedrons.at(0);
  auto l_minimumStress = a_tetrahedrons.at(0)->GetStress();

  for (auto tetraIterator = 1; tetraIterator < a_tetrahedrons.size(); ++tetraIterator)
  {
    auto l_stress = a_tetrahedrons.at(tetraIterator)->GetStress();
    int l_numberOfMinRows = 0;

    for (auto lIndex = 0; lIndex < l_stress.GetRows(); ++lIndex)
    {
      for (auto cIndex = 0; cIndex < l_stress.GetColumns(); ++cIndex)
      {
        auto l_currentNode = l_stress.ClipVector(lIndex, cIndex);
        auto l_minNode = l_minimumStress.ClipVector(lIndex, cIndex);

        if (l_currentNode.x() < l_minNode.x() && l_currentNode.y() < l_minNode.y() && l_currentNode.z() < l_minNode.z())
        {
          l_numberOfMinRows++;
        }
      }
    }

    if (l_numberOfMinRows == l_stress.GetRows())
    {
      l_minimumStress = l_stress;
      
      l_leastStressedTetra = a_tetrahedrons.at(tetraIterator);
    }
  }

  return l_leastStressedTetra;
}

std::shared_ptr<chrono::fea::ChElementTetra_4> getMostStressedTetra(const std::vector<std::shared_ptr<chrono::fea::ChElementTetra_4>>& a_tetrahedrons)
{
  auto l_mostStressedTetra = a_tetrahedrons.at(0);
  auto l_maximumStress = a_tetrahedrons.at(0)->GetStress();

  for (auto tetraIterator = 1; tetraIterator < a_tetrahedrons.size(); ++tetraIterator)
  {
    auto l_stress = a_tetrahedrons.at(tetraIterator)->GetStress();
    int l_numberOfMinRows = 0;

    for (auto lIndex = 0; lIndex < l_stress.GetRows(); ++lIndex)
    {
      for (auto cIndex = 0; cIndex < l_stress.GetColumns(); ++cIndex)
      {
        auto l_currentNode = l_stress.ClipVector(lIndex, cIndex);
        auto l_maxNode = l_maximumStress.ClipVector(lIndex, cIndex);

        if (l_currentNode.x() >= l_maxNode.x() && l_currentNode.y() >= l_maxNode.y() && l_currentNode.z() >= l_maxNode.z())
        {
          l_numberOfMinRows++;
        }
      }
    }

    if (l_numberOfMinRows == l_stress.GetRows())
    {
      l_maximumStress = l_stress;

      l_mostStressedTetra = a_tetrahedrons.at(tetraIterator);
    }
  }

  return l_mostStressedTetra;
}

void logStressedTetrahedrons(const std::shared_ptr<chrono::fea::ChElementTetra_4>& a_leastStressed, const std::shared_ptr<chrono::fea::ChElementTetra_4>& a_mostStressed)
{
  std::ofstream l_logFile("stress.txt");
  chrono::ChMatrixDynamic<double> nodes;

  l_logFile << " Least strained tetra:" << std::endl;
  a_leastStressed->GetStateBlock(nodes);

  for (auto lIndex = 0; lIndex < nodes.GetRows(); ++lIndex)
  {
    for (auto cIndex = 0; cIndex < nodes.GetColumns(); ++cIndex)
    {
      auto l_nodeCoord = nodes.ClipVector(lIndex, cIndex);

      l_logFile << l_nodeCoord.x() << " " << l_nodeCoord.y() << " " << l_nodeCoord.z() << std::endl;
    }
  }

  l_logFile << "\nMost strained tetra:" << std::endl;
  a_mostStressed->GetStateBlock(nodes);

  for (auto lIndex = 0; lIndex < nodes.GetRows(); ++lIndex)
  {
    for (auto cIndex = 0; cIndex < nodes.GetColumns(); ++cIndex)
    {
      auto l_nodeCoord = nodes.ClipVector(lIndex, cIndex);

      l_logFile << l_nodeCoord.x() << " " << l_nodeCoord.y() << " " << l_nodeCoord.z() << std::endl;
    }
  }
}