#include <environment/Beam.h>
#include <environment/Utils.h>

#include <chrono/fea/ChBeamSection.h>
#include <chrono/fea/ChBuilderBeam.h>
#include <chrono/fea/ChMesh.h>
#include <chrono/fea/ChMeshFileLoader.h>
#include <chrono/fea/ChVisualizationFEAmesh.h>
#include <chrono/physics/ChContinuumMaterial.h>
#include <chrono_irrlicht/ChIrrApp.h>
#include <chrono/fea/ChNodeFEAxyz.h>
#include <chrono/fea/ChElementTetra_4.h>
#include <chrono/core/ChMatrixDynamic.h>

#include <iostream>
#include <string>

Beam::Beam()
{
  m_beam = std::make_shared< chrono::fea::ChMesh>();
  beamGenerator();
}

std::shared_ptr<chrono::fea::ChMesh> Beam::getBeam() const
{
  return m_beam;
}

std::vector<std::shared_ptr<chrono::fea::ChElementTetra_4>>  Beam::getTetrahedrons() const
{
  return m_tetrahedrons;
}

void Beam::addContactSurface(std::shared_ptr<chrono::fea::ChContactSurfaceMesh>& a_contactSurface)
{
  m_beam->AddContactSurface(a_contactSurface);
}

void Beam::applyForceOnANode()
{
  auto l_lastBeamNode = std::dynamic_pointer_cast<chrono::fea::ChNodeFEAxyz>(m_beam->GetNode(m_beam->GetNnodes() - 1));
  l_lastBeamNode->SetForce(g_activeForceOnABeamNode);
}

void Beam::visualizeBeam()
{
  auto mvisualizemesh = std::make_shared<chrono::fea::ChVisualizationFEAmesh>(*(m_beam.get()));
  mvisualizemesh->SetFEMdataType(chrono::fea::ChVisualizationFEAmesh::E_PLOT_ELEM_STRAIN_VONMISES);
  mvisualizemesh->SetColorscaleMinMax(-0.5, 0.50);
  mvisualizemesh->SetShrinkElements(true, 0.85);
  mvisualizemesh->SetSmoothFaces(true);

  m_beam->AddAsset(mvisualizemesh);
}

void Beam::updateStrainedTetrahedronDimensions(const std::shared_ptr<chrono::fea::ChElementTetra_4>& a_leastStrained, const std::shared_ptr<chrono::fea::ChElementTetra_4>& a_mostStrained)
{
  const chrono::ChVector<double> c_updateVector = chrono::ChVector<double>(0.1, 0.1, 0.1);

  for (auto iterator : m_tetrahedrons)
  {
    if (iterator == a_leastStrained)
    {
      chrono::ChMatrixDynamic<double> l_nodes;
      iterator->GetStateBlock(l_nodes);

      for (auto lIndex = 0; lIndex < l_nodes.GetRows(); ++lIndex)
      {
        for (auto cIndex = 0; cIndex < l_nodes.GetColumns(); ++cIndex)
        {
          auto l_currentNode = l_nodes.ClipVector(lIndex, cIndex);

          l_currentNode.Sub(l_currentNode, c_updateVector);
        }
      }
    }

    if (iterator == a_mostStrained)
    {
      chrono::ChMatrixDynamic<double> l_nodes;
      iterator->GetStateBlock(l_nodes);

      for (auto lIndex = 0; lIndex < l_nodes.GetRows(); ++lIndex)
      {
        for (auto cIndex = 0; cIndex < l_nodes.GetColumns(); ++cIndex)
        {
          auto l_currentNode = l_nodes.ClipVector(lIndex, cIndex);

          l_currentNode.Add(l_currentNode, c_updateVector);
        }
      }
    }
  }
}

void Beam::beamGenerator()
{
  std::vector<std::shared_ptr<chrono::fea::ChNodeFEAxyz>> l_nodes;
  std::vector<std::vector<int>> l_elements;

  readNodesFromFile(l_nodes);
  readElementsFromFile(l_elements);

  auto l_material = std::make_shared<chrono::fea::ChContinuumPlasticVonMises>();
  l_material->Set_E(0.01e9);
  l_material->Set_v(0.3);
  l_material->Set_RayleighDampingK(0.000);
  l_material->Set_density(000);

  for (size_t index = 0; index < g_noTetahedrons * g_beamBoxes ; index++)
  {
    auto l_node1 = l_nodes[l_elements[index][1]];
    auto l_node2 = l_nodes[l_elements[index][2]];
    auto l_node3 = l_nodes[l_elements[index][3]];
    auto l_node4 = l_nodes[l_elements[index][4]];

    m_beam->AddNode(l_node1);
    m_beam->AddNode(l_node2);
    m_beam->AddNode(l_node3);
    m_beam->AddNode(l_node4);

    auto l_element = std::make_shared<chrono::fea::ChElementTetra_4>();
    l_element->SetNodes(l_node1, l_node2, l_node3, l_node4);
    l_element->SetMaterial(l_material);

    m_beam->AddElement(l_element);

    m_tetrahedrons.push_back(l_element);
  }
}

void Beam::readNodesFromFile(std::vector<std::shared_ptr<chrono::fea::ChNodeFEAxyz>>& a_nodes)
{
  chrono::ChCoordsys<> l_rotation(chrono::VNULL, chrono::Q_from_AngAxis(chrono::CH_C_PI_2, chrono::VECT_X));
  chrono::ChCoordsys<> l_yDisp(g_beamStartPosition);
  chrono::ChCoordsys<> l_position = l_rotation >> l_yDisp;
  chrono::ChMatrix33<> l_matrixTransform(l_position.rot);

  std::string l_line;
  std::ifstream l_file(g_beamNodesFile);
  int index = 0;
  if (l_file.is_open())
  {
    while (getline(l_file, l_line))
    {
      if (index > 4)
      {
        std::stringstream l_stringStream(l_line);
        std::string l_coord;
        std::vector<double> l_coords;
        while (getline(l_stringStream, l_coord, ' '))
          l_coords.push_back(std::stod(l_coord));

        auto l_node = std::make_shared<chrono::fea::ChNodeFEAxyz>(l_position.pos + l_matrixTransform * chrono::ChVector<>(l_coords[1], l_coords[2], l_coords[3]));
        a_nodes.push_back(l_node);
      }
      index++;
    }
    l_file.close();
  }
  else 
  {
    std::cout << "Unable to open file beam.node\n";
    exit(-1);
  }
}

void Beam::readElementsFromFile(std::vector<std::vector<int>>& a_elements)
{
  std::string l_line;
  std::ifstream l_file(g_beamElementsFile);
  int index = 0;
  if (l_file.is_open())
  {
    while (getline(l_file, l_line))
    {
      if (index > 4)
      {
        std::stringstream l_stringStream(l_line);
        std::string l_nodeIndex;
        std::vector<int> l_element;
        while (getline(l_stringStream, l_nodeIndex, ' '))
          l_element.push_back(std::stoi(l_nodeIndex) - 1);

        a_elements.push_back(l_element);
      }
      index++;
    }
    l_file.close();
  }
  else std::cout << "Unable to open file beam.ele\n";
}