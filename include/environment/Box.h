#pragma once
#include <memory> 

#include <chrono/fea/ChMesh.h>
#include <chrono/physics/ChBodyEasy.h>
#include <chrono/physics/ChSystemNSC.h>
#include <chrono/fea/ChNodeFEAxyz.h>
#include <chrono_irrlicht/ChIrrApp.h>

class Box
{
public:
	Box();
	~Box() = default;

	void attachToBeam(std::shared_ptr<chrono::fea::ChMesh>& a_beam, chrono::ChSystemNSC& l_chronoSystem);

private:
	void createLink(chrono::ChSystemNSC& a_chronoSystem, const std::shared_ptr<chrono::fea::ChNodeFEAxyz>& a_node, std::shared_ptr<chrono::fea::ChMesh>& a_mesh);

private:
	std::shared_ptr<chrono::ChBodyEasyBox> m_box;
	std::shared_ptr<chrono::fea::ChMesh> m_cable;
};
