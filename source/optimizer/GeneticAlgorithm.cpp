#include <optimizer/GeneticAlgorithm.h>
#include <optimizer/UtilsOptimizer.h>
#include <random>
#include <fstream>

void GeneticAlgorithm::run(std::string a_filename, const uint16_t& a_individualsSize, const bool& a_isMaximizationRequired)
{
  m_population.generatePopulation(a_individualsSize);

  std::ofstream l_file;
  l_file.open(a_filename, std::ios::out);
  l_file << "#start" << std::endl;

  for (auto l_epoch = 0; l_epoch < UtilsOptimizer::g_epochs; ++l_epoch)
  {
    select(a_individualsSize, a_isMaximizationRequired);
    crossover(a_individualsSize, a_isMaximizationRequired);
    mutate(a_individualsSize, a_isMaximizationRequired);

    l_file << "#generationNo: " << l_epoch << std::endl;
    for (const auto& l_individual : m_population.getPop())
    {
      l_file << "Chromosome ";
      for (auto l_index = 0; l_index < a_individualsSize; ++l_index)
      {
        l_file << l_individual.getChromosome(l_index) << " ";
      }
      l_file << std::endl;
      l_file << "Value " << l_individual.getFitnessValue() << std::endl;
    }
  }

  l_file << "#end" << std::endl;

  if (a_individualsSize == 1)
  {
    m_population.getExpectedIndividual().printChromosome(0);
    std::cout << "x = " << m_population.getExpectedIndividual().getValueAcordingToInteval(0, a_individualsSize) << std::endl;

    l_file << "Maximum/Minimum chromosome: " << m_population.getExpectedIndividual().getChromosome(0) << std::endl;

    std::cout << "f(x) = " << m_population.getExpectedIndividual().getFitnessValue() << std::endl;
  }
  else
  {
    m_population.getExpectedIndividual().printChromosome(0);
    m_population.getExpectedIndividual().printChromosome(1);
    std::cout << "x = " << m_population.getExpectedIndividual().getValueAcordingToInteval(0, a_individualsSize) << std::endl;
    std::cout << "y = " << m_population.getExpectedIndividual().getValueAcordingToInteval(1, a_individualsSize) << std::endl;

    l_file << "Maximum/Minimum chromosome: "
      << m_population.getExpectedIndividual().getChromosome(0) << " "
      << m_population.getExpectedIndividual().getChromosome(1) << std::endl;

    std::cout << "f(x, y) = " << m_population.getExpectedIndividual().getFitnessValue() << std::endl;
  }

  l_file << "Maximum/Minimum value: " << m_population.getExpectedIndividual().getFitnessValue() << std::endl;
}

void GeneticAlgorithm::select(const uint16_t& a_individualsSize, const bool& a_isMaximizationRequired)
{
  m_population.calculateIndividualFitness(a_individualsSize);
  m_population.calculateIndividualSelectionProbability(a_isMaximizationRequired);
  m_population.calculateIndividualSelectionCumulativeProbability();

  std::vector<Individual> l_individualForTheNextGeneration = m_population.getSuitableIndividualsForTheNewGeneration(a_isMaximizationRequired);
  m_population.setPopulation(l_individualForTheNextGeneration);

  m_population.storeTheBestResult(a_isMaximizationRequired);
}

void GeneticAlgorithm::crossover(const uint16_t& a_individualsSize, const bool& a_isMaximizationRequired)
{
  std::vector<double> l_randomValues;
  for (auto l_index = 0; l_index < UtilsOptimizer::g_populationSize; ++l_index)
  {
    l_randomValues.push_back(UtilsOptimizer::g_randomRealValue(UtilsOptimizer::g_generator));
  }

  m_population.crossover(l_randomValues, a_individualsSize);
}

void GeneticAlgorithm::mutate(const uint16_t & a_individualsSize, const bool& a_isMaximizationRequired)
{
  m_population.mutate(a_individualsSize);
}


