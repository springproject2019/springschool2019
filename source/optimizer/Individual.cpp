#include "optimizer\Individual.h"

void Individual::setChromosomeGene(const int& a_chromosomeIndex, const int& a_genePosition, const char& a_geneValue)
{
  m_chromosomes[a_chromosomeIndex].setGene(a_genePosition, a_geneValue);
}

char Individual::getChromosomeGene(const int& a_chromosomeIndex, const int& a_genePosition) const
{
  return m_chromosomes[a_chromosomeIndex].getGene(a_genePosition);
}

std::string Individual::getChromosome(const int& a_chromosomeIndex) const
{
  return m_chromosomes[a_chromosomeIndex].getChromosome();
}

void Individual::setFitnessValue(const double& a_value)
{
  m_fitnessValue = a_value;
}

double Individual::getValueAcordingToInteval(const int& a_chromosomeIndex, const int& a_individualsSize) const
{
  if (a_individualsSize == 1)
  {
    return m_chromosomes[a_chromosomeIndex].getValueAcordingToInteval(
      UtilsOptimizer::g_lowestValueFromInterval,
      UtilsOptimizer::g_highestValueFromInterval);
  }

  if (a_chromosomeIndex == 0)
  {
    return m_chromosomes[a_chromosomeIndex].getValueAcordingToInteval(
      UtilsOptimizer::g_lowestValueFromIntervalX,
      UtilsOptimizer::g_highestValueFromIntervalX);
  }

  return m_chromosomes[a_chromosomeIndex].getValueAcordingToInteval(
    UtilsOptimizer::g_lowestValueFromIntervalY,
    UtilsOptimizer::g_highestValueFromIntervalY);
}

double Individual::getFitnessValue() const
{
  return m_fitnessValue;
}

void Individual::setSelectionProbability(const double& a_value)
{
  m_selectionProbability = a_value;
}

double Individual::getSelectionProbability() const
{
  return m_selectionProbability;
}

void Individual::setSelectionCumulativeProbability(const double& a_value)
{
  m_selectionCumulativeProbability = a_value;
}

void Individual::printChromosome(const int& a_chromosomeIndex) const
{
  m_chromosomes[a_chromosomeIndex].printChromosome();
}

double Individual::getSelectionCumulativeProbability() const
{
  return m_selectionCumulativeProbability;
}