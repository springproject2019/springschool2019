#include <optimizer/Population.h>
#include <optimizer/UtilsOptimizer.h>

void Population::generatePopulation(const uint16_t& a_individualsSize)
{
  for (auto l_index = 0; l_index < UtilsOptimizer::g_populationSize; ++l_index)
  {
    Individual l_individual{};

    for (auto l_indexIndiv = 0; l_indexIndiv < a_individualsSize; ++l_indexIndiv)
    {
      for (auto l_position = 0; l_position < UtilsOptimizer::g_chromosomeSize; ++l_position)
      {
        char l_char = UtilsOptimizer::g_randomBinaryValue(UtilsOptimizer::g_generator) + '0';
        l_individual.setChromosomeGene(l_indexIndiv, l_position, l_char);
      }
    }

    m_population.push_back(l_individual);
  }
}

void Population::calculateIndividualFitness(const uint16_t& a_individualsSize)
{
  for (auto& l_individual : m_population)
  {
    if (a_individualsSize == 1)
    {
      double l_x = l_individual.getValueAcordingToInteval(0, a_individualsSize);
      auto l_fitnessValue = UtilsOptimizer::g_functionSingleVariable(l_x);

      l_individual.setFitnessValue(l_fitnessValue);
    }
    else
    {
      double l_x = l_individual.getValueAcordingToInteval(0, a_individualsSize);
      double l_y = l_individual.getValueAcordingToInteval(1, a_individualsSize);
      auto l_fitnessValue = UtilsOptimizer::g_functionTwoVariables(l_x, l_y);

      l_individual.setFitnessValue(l_fitnessValue);
    }
  }
}

double Population::getSumIndividualFitness() const
{
  double l_sum = 0;
  for (const auto& l_individual : m_population)
  {
    l_sum += std::pow(M_E, l_individual.getFitnessValue());
  }

  return l_sum;
}


double Population::getMaxValue(const double a_value) const
{
  return a_value > 0 ? a_value : 0;
}

void Population::calculateIndividualSelectionProbability(const bool& a_isMaximizationRequired)
{
  const auto l_sum = getSumIndividualFitness();
  for (auto& l_individual : m_population)
  {
    double l_selectionProbability = 0.;
    if (a_isMaximizationRequired == true)
    {
      l_selectionProbability = std::abs(std::pow(M_E, l_individual.getFitnessValue()) / l_sum);
    }
    else
    {
      l_selectionProbability = getMaxValue(-l_individual.getFitnessValue());
    }
    l_individual.setSelectionProbability(l_selectionProbability);
  }
}

void Population::calculateIndividualSelectionCumulativeProbability()
{
  for (auto l_indexIndividual = 0; l_indexIndividual < m_population.size(); ++l_indexIndividual)
  {
    double l_selectionCumulativeProbability = 0;
    for (auto l_index = 0; l_index <= l_indexIndividual; ++l_index)
    {
      l_selectionCumulativeProbability += m_population[l_index].getSelectionProbability();
    }

    m_population[l_indexIndividual].setSelectionCumulativeProbability(l_selectionCumulativeProbability);
  }
}

std::vector<Individual> Population::getSuitableIndividualsForTheNewGeneration(const bool& a_isMaximizationRequired) const
{
  std::vector<Individual> l_individuals;
  double l_value{};

  for (auto l_index = 0; l_index < m_population.size(); ++l_index)
  {
    l_value = UtilsOptimizer::g_randomRealValue(UtilsOptimizer::g_generator);

    for (auto l_indexNext = 0; l_indexNext < m_population.size() - 1; ++l_indexNext)
    {
      if (m_population[l_indexNext].getSelectionCumulativeProbability() >= l_value && l_value >= 0)
      {
        l_individuals.push_back(m_population[l_indexNext]);
        break;
      }
      else if (m_population[l_indexNext].getSelectionCumulativeProbability() <= l_value && l_value <= m_population[l_indexNext + 1].getSelectionCumulativeProbability())
      {
        l_individuals.push_back(m_population[l_indexNext + 1]);
        break;
      }
    }

  }

  return l_individuals;
}

Individual Population::getExpectedIndividual() const
{
  return m_individual;
}

void Population::setPopulation(const std::vector<Individual> & a_individuals)
{
  m_population.clear();
  m_population = a_individuals;
}

void Population::crossover(const std::vector<double> & a_randomValues, const uint16_t & a_individualsSize)
{
  std::vector<int> l_individuals = getIndividualsForCrossingover(a_randomValues);

  if (l_individuals.size() == 0)
  {
    return;
  }

  for (auto l_index = 0; l_index < l_individuals.size() - 1; l_index += 2)
  {
    crossTwoChromosomes(m_population[l_individuals[l_index]], m_population[l_individuals[l_index + 1]], a_individualsSize);
  }
}

void Population::crossTwoChromosomes(Individual & a_firstChromosome, Individual & a_secondChromosome, const uint16_t & a_individualsSize)
{
  const int t = UtilsOptimizer::g_randomIntegerValue(UtilsOptimizer::g_generator);

  for (auto l_indexChrom = 0; l_indexChrom < a_individualsSize; ++l_indexChrom)
  {
    for (auto l_index = t; l_index < UtilsOptimizer::g_chromosomeSize; ++l_index)
    {
      const char l_firstGene = a_firstChromosome.getChromosomeGene(l_indexChrom, l_index);
      const char l_secondGene = a_secondChromosome.getChromosomeGene(l_indexChrom, l_index);

      a_firstChromosome.setChromosomeGene(l_indexChrom, l_index, l_secondGene);
      a_secondChromosome.setChromosomeGene(l_indexChrom, l_index, l_firstGene);
    }
  }
}

std::vector<int> Population::getIndividualsForCrossingover(const std::vector<double> & a_randomValues)
{
  std::vector<int> l_individualsIndex;
  for (auto l_index = 0; l_index < m_population.size(); ++l_index)
  {
    if (a_randomValues[l_index] < UtilsOptimizer::g_crossoverProbability)
    {
      l_individualsIndex.push_back(l_index);
    }
  }

  if (l_individualsIndex.size() && l_individualsIndex.size() % 2 == 1)
  {
    l_individualsIndex.pop_back();
  }

  return l_individualsIndex;
}

void Population::mutate(const uint16_t & a_individualsSize)
{
  for (auto& l_individual : m_population)
  {
    for (auto l_indexChrom = 0; l_indexChrom < a_individualsSize; ++l_indexChrom)
    {
      for (auto l_index = 0; l_index < UtilsOptimizer::g_chromosomeSize; ++l_index)
      {
        const double l_mutationProbability = UtilsOptimizer::g_randomRealValue(UtilsOptimizer::g_generator);
        if (l_mutationProbability < UtilsOptimizer::g_mutationProbability)
        {
          l_individual.setChromosomeGene(l_indexChrom, l_index, l_individual.getChromosomeGene(l_indexChrom, l_index) == '1' ? '0' : '1');
        }
      }
    }
  }
}

void Population::storeTheBestResult(const bool& a_isMaximizationRequired)
{
  for (auto& l_individual : m_population)
  {
    if (a_isMaximizationRequired == true)
    {
      if (l_individual.getFitnessValue() > m_value)
      {
        m_value = l_individual.getFitnessValue();
        m_individual = l_individual;
      }
    }
    else
    {
      if (l_individual.getFitnessValue() < m_value)
      {
        m_value = l_individual.getFitnessValue();
        m_individual = l_individual;
      }
    }
  }
}
